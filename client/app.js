import '../imports/api/client'

Template.body.helpers({

  cinemas: function() {
    return Cinemas.find();
  }

});

// adds index to each item
UI.registerHelper('indexedArray', function(context, options) {
  if (context) {
    return context.map(function(item, index) {
      item._index = index;
      return item;
    });
  }
});