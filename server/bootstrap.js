// run this when the meteor app is started
Meteor.startup(function() {

  if (Users.find().count() === 0) {
    var sampleUsers = [
      {
        firstName: 'John',
        lastName: 'Guest',
        email: 'guest@gmail.com',
        password: '111',
        type: 'guest'
      },
      {
        firstName: 'Mary',
        lastName: 'Admin',
        email: 'admin@gmail.com',
        password: 'admin',
        type: 'admin'
      }
    ];

    // loop over each sample poll and insert into database
    _.each(sampleUsers, function(user) {
      Users.insert(user);
    });
  }

  if (Movies.find().count() === 0) {
    var sampleMovies = [
      {
        name: 'Baby Driver',
        sypnosis: 'A talented, young getaway driver (Ansel Elgort) relies on the beat of his personal soundtrack to be the best in the game. When he meets the girl of his dreams (Lily James), Baby sees a chance to ditch his criminal life and make a clean getaway. But after being coerced into working for a crime boss (Kevin Spacey), he must face the music when a doomed heist threatens his life, love and freedom.',
        runningTime: '1 hour and 53 minutes',
        genre: 'Action',
        cast: 'Ansel Elgort, Lily James, Kevin Spacey',
        releaseDate: 'August 2, 2016',
        image: 'http://tickets.smcinema.com/BookingCMS/Images/baby driver_20170630191338.png',
        availability: [
          {
            branch: 'Manila',
            cinemas: [
              {
                name: 'Cinema 1',
                seats: 11,
                dateTime: [
                  { date:'Aug 2', time:'10:30 AM' },
                  { date:'Aug 2', time:'7:30 PM' },
                  { date:'Aug 3', time:'4:30 PM' }
                ]
              }
            ]
          },
          {
            branch: 'Makati',
            cinemas: [
              {
                name: 'Cinema 2',
                seats: 33,
                dateTime: [
                  { date:'Aug 2', time:'11:00 AM' },
                  { date:'Aug 2', time:'8:00 PM' },
                  { date:'Aug 3', time:'5:00 PM' }
                ]
              }
            ]
          }
        ],
        type: "Now Showing",
        price: 215
      },
      {
        name: 'Eden',
        sypnosis: 'When a US soccer team gets stranded on a deserted island after a plane crash they must face difficult choices to survive. Modern day Lord of the Flies.',
        runningTime: '1 hour and 30 minutes',
        genre: 'Thriller',
        cast: 'James Remar, Jessica Lowndes, Sung Kang',
        releaseDate: 'August 24, 2016',
        image: 'http://tickets.smcinema.com/BookingCMS/Images/EDEN_20160818135445.jpg',
        availability: [
          {
            branch: 'Manila',
            dateTime: [
              'Aug 1 - 2:00 PM',
              'Aug 1 - 7:00 PM',
              'Aug 2 - 11:00 AM'
            ]
          },
          {
            branch: 'Makati',
            dateTime: [
              'Aug 1 - 1:00 PM',
              'Aug 1 - 5:00 PM',
              'Aug 2 - 4:00 PM'
            ]
          }
        ],
        type: "Now Showing",
        price: 250
      },
      {
        name: 'Cars 3',
        sypnosis: "Blindsided by a new generation of blazing-fast racers, the legendary Lightning McQueen is suddenly pushed out of the sport he loves. To get back in the game, he will need the help of an eager young race technician with her own plan to win, inspiration from the late Fabulous Hudson Hornet, and a few unexpected turns. Proving that #95 isn't through yet will test the heart of a champion on Piston Cup Racing's biggest stage!",
        runningTime: '2 hours and 3 minutes',
        genre: 'Animation',
        cast: 'Owen Wilson, Armie Hammer, Cristela Alonzo',
        releaseDate: 'August 4, 2016',
        image: 'http://tickets.smcinema.com/BookingCMS/Images/Cars3_20170718111750.jpg',
        availability: [
          {
            branch: 'Manila',
            dateTime: [
              'Aug 4 - 2:00 PM',
              'Aug 4 - 7:00 PM',
              'Aug 5 - 11:00 AM'
            ]
          },
          {
            branch: 'Makati',
            dateTime: [
              'Aug 4 - 1:00 PM',
              'Aug 4 - 5:00 PM',
              'Aug 5 - 4:00 PM'
            ]
          }
        ],
        type: "Now Showing",
        price: 300
      },
      {
        name: 'The Dark Tower',
        sypnosis: "The last Gunslinger, Roland Deschain (Idris Elba), has been locked in an eternal battle with Walter O'Dim, also known as the Man in Black (Matthew McConaughey), determined to prevent him from toppling the Dark Tower, which holds the universe together. With the fate of the worlds at stake, good and evil will collide in the ultimate battle as only Roland can defend the Tower from the Man in Black.",
        runningTime: '1 hours and 40 minutes',
        genre: 'Action/Adventure',
        cast: 'Matthew McConaughey, Idris Elba',
        releaseDate: 'August 2, 2017',
        image: 'http://tickets.smcinema.com/BookingCMS/Images/THE DARK TOWER VERTICAL_20170727105902.png',
        availability: [
          {
            branch: 'Manila',
            dateTime: [
              'Aug 2 - 2:00 PM',
              'Aug 2 - 7:00 PM',
              'Aug 3 - 11:00 AM'
            ]
          },
          {
            branch: 'Makati',
            dateTime: [
              'Aug 2 - 1:00 PM',
              'Aug 2 - 5:00 PM',
              'Aug 3 - 4:00 PM'
            ]
          }
        ],
        type: "Now Showing",
        price: 300
      },
      {
        name: 'Fan Girl Fan Boy',
        sypnosis: "A teen-oriented romance-comedy about a gwapong-gwapo-sa-sarili wannabe actor (Julian Trono) and the talented teleserye dubber (Ella Cruz) who agrees to help him improve his acting. Her coaching actually helps him achieve his dream of becoming a star, only to find herself heartbroken when complications arise in his career.",
        runningTime: '1 hours and 45 minutes',
        genre: 'Romance/Comedy',
        cast: 'Julian Trono, Ella Cruz',
        releaseDate: 'September 2, 2017',
        image: 'http://tickets.smcinema.com/BookingCMS/Images/fan girl fan boy_20170722131553.jpg',
        availability: [
          {
            branch: 'Manila',
            dateTime: [
              'Sept 2 - 2:00 PM',
              'Sept 2 - 7:00 PM',
              'Sept 3 - 11:00 AM'
            ]
          },
          {
            branch: 'Makati',
            dateTime: [
              { 'date':'Sept 2', 'time':'1:00 PM' },
              { 'date':'Sept 2', 'time':'3:00 PM' },
              { 'date':'Sept 3', 'time':'1:00 PM' }
            ]
          }
        ],
        type: "Comming Soon",
        price: 600
      },
      {
        name: 'Fairy Tail: Dragon Cry',
        sypnosis: "The Dragon Cry is a magic artefact with enough power to destroy the world.  Enshrined within a temple in the Kingdom of Fiore, the Dragon Cry is stolen by Zash, a traitor of Fiore, and delivered to Animus, the ruler of the Kingdom of Stella. The task of recapturing the Dragon Cry falls to Natsu and the Wizards of the Fairy Tail Guild.",
        runningTime: '1 hours and 25 minutes',
        genre: 'Anime',
        cast: 'Aya Hirano, Rie Kugimiya, Satomi Satou',
        releaseDate: 'August 9, 2017',
        image: 'http://tickets.smcinema.com/BookingCMS/Images/fairy tale dragon cry_20170719150736.png',
        availability: [
          {
            branch: 'Manila',
            dateTime: [
              'Aug 9 - 2:00 PM',
              'Aug 9 - 7:00 PM',
              'Aug 10 - 11:00 AM'
            ]
          },
          {
            branch: 'Makati',
            dateTime: [
              'Aug 9 - 1:00 PM',
              'Aug 9 - 5:00 PM',
              'Aug 10 - 4:00 PM'
            ]
          }
        ],
        type: "Comming Soon",
        price: 450
      }
    ];

    // loop over each sample poll and insert into database
    _.each(sampleMovies, function(movie) {
      Movies.insert(movie);
    });
  }

  if (Branches.find().count() === 0) {
    var sampleBranches = [
      {
        branch: 'Makati',
        cinemas: [
          {
            name: 'cinema 1',
            seats: 49,
            timeSlot: [
              '10:30 AM',
              '1:30 PM',
              '4:30 PM',
              '7:30 PM',
              '10:30 PM'
            ]
          },
          {
            name: 'cinema 2',
            seats: 33,
            timeSlot: [
              '11:00 AM',
              '2:00 PM',
              '5:00 PM',
              '8:00 PM',
              '11:00 PM'
            ]
          },
          {
            name: 'cinema 3',
            seats: 12,
            timeSlot: [
              '11:00 AM',
              '2:00 PM',
              '5:00 PM',
              '8:00 PM',
              '11:00 PM'
            ]
          }
        ]
      },
      {
        branch: 'Manila',
        cinemas: [
          {
            name: 'cinema 1',
            seats: 11,
            timeSlot: [
              '10:30 AM',
              '1:30 PM',
              '4:30 PM',
              '7:30 PM',
              '10:30 PM'
            ]
          },
          {
            name: 'cinema 2',
            seats: 22,
            timeSlot: [
              '11:00 AM',
              '2:00 PM',
              '5:00 PM',
              '8:00 PM',
              '11:00 PM'
            ]
          }
        ]
      }
    ];

    // loop over each sample poll and insert into database
    _.each(sampleBranches, function(branch) {
      Branches.insert(branch);
    });
  }

});