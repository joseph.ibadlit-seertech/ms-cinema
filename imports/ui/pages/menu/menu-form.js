import './menu-form.html';
import './menu-form.css';
import './login-form.js';
import './register-form.js';
import '../movie-gallery/movie-gallery-form.html'

Template.menu.helpers({
	'currentUser': function(){
	    return Session.get('currentUser') || { firstName:'Guest' };
	},
	'isAdmin': function(){
		return (Session.get('currentUser') && Session.get('currentUser').type == 'admin');
	}
});

Template.menu.events({
	'click #logout': function(events){
		event.preventDefault();
		Session.set('currentUser', null);
		alert('Logout successfully!');
		Router.go('/home');
	},
	'click #account': function(events) {
		event.preventDefault();
		Router.go('/account');
	}
});