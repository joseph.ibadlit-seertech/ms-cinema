import './login-form.css'
import './login-form.html'

Template.login.events({

  // handle the form submission
  'submit form': function(event) {

    // stop the form from submitting
    event.preventDefault();

    // get the data we need from the form
    var user = {
      email: event.target.email.value,
      password: event.target.password.value
    };    

    console.log('user',user)
    // create the new poll
    var confirmed = Users.find(user).fetch();
    console.log('confirmed',confirmed);

    if (confirmed.length == 0) {
    	alert('Incorrect email or password. Please try again!');
    } else {
    	console.log('confirmed[0].firstName')
    	Session.set('currentUser', { '_id':confirmed[0]._id, 'firstName':confirmed[0].firstName, 'type':confirmed[0].type } );
    	alert('Login successfully.')
    }
  }

});