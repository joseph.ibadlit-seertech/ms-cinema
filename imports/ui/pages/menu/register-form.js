import './register-form.css';
import './register-form.html';

Template.register.events({

  // handle the form submission
  'submit form': function(event) {

    // stop the form from submitting
    event.preventDefault();
    var email = event.target.email.value;
    console.log('email', email);
    var emailExist = Users.find({ 'email':email }).fetch();
    console.log('emailExist', emailExist);
    if (event.target.password.value != event.target['confirm-password'].value) {
    	alert("Password did not match. Please try again.")
    } else if (emailExist.length != 0) {
    	alert("Email already exist. Please use another email.");
    } else {
    	var user = {
    		'firstName': event.target.firstName.value,
    		'lastName': event.target.lastName.value,
    		'email': event.target.email.value,
    		'password': event.target.password.value,
    		'type': 'guest'
    	};
    	var result = Users.insert(user);
    	console.log('result',result);
    	if (result) {
    		Router.go('/home');
    		alert('Successfully sign up. Please login.');
    	} else {
    		alert('Encountered error while signing up. Please try again.');
    	}
    }
  }

});