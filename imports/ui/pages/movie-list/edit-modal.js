import './edit-modal.html';

Template.editModal.helpers({
	'movie': function () {
		// console.log('helper editModal movieId', Session.get('movieId'))
		if (Session.get('movieId')) {
			var movie = Movies.findOne({'_id':Session.get('movieId')});
			console.log('releaseDate',movie.releaseDate);
			return movie
		}

		return null
	}
})

Template.editModal.events({
  'submit form': function(event) {
  	event.preventDefault();
  	console.log('submit form editModal');
  	var movie = {
  		'name':event.target.name,
  		'sypnosis':event.target.sypnosis
  	}

  	console.log('movie dict', movie)
  }
})
