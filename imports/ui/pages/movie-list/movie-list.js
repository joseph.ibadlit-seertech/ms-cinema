import './movie-list.html';
import './edit-modal.js';


Template.movieList.helpers({
	'movieList': function() {
		return Movies.find({})
	},
	'movieId': function() {
		console.log('movieId helper movieList')
		return Session.get('movieId') || 'none'
	}
})


Template.movieList.events({
  'click #btnEdit': function(event) {
  	event.preventDefault();
  	Session.set('movieId', this._id);
  	console.log('edit movieId', Session.get('movieId'))
  },
  'click #btnDelete': function(event) {
    // prevent the default behavior
    event.preventDefault();
    console.log('btnDelete');
    console.log('this',this._id);
    Movies.remove({'_id':this._id});
    alert('Movie deleted successfully.') 
  }
})