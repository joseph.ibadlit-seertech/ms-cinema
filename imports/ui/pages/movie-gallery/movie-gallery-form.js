import './movie-gallery-form.html'
import './movie-gallery-form.css'

Template.movieGallery.helpers({

  movies: function() {
  	console.log('movies!!!')
  	var type = Session.get('movieType') || 'Now Showing';
    return Movies.find({ 'type':type});
  }

});

Template.movieGallery.events({
  'click #now-showing': function(event) {
    // prevent the default behavior
    event.preventDefault();

    Session.set('movieType','Now Showing')
  },
  'click #comming-soon': function(event) {
    // prevent the default behavior
    event.preventDefault();

    Session.set('movieType','Comming Soon')
  },
  'click #buy-ticket': function(event) {
    // prevent the default behavior
    event.preventDefault();
    console.log('id', $(event.currentTarget).attr('data-id'))
    if (Session.get('currentUser')) {
    	Session.set('processTicket', 0)
    	Router.go('/schedule/' + $(event.currentTarget).attr('data-id'));
    } else {
    	alert('Please login first.');
    }
  },
  'click #reserve-ticket': function(event) {
    // prevent the default behavior
    event.preventDefault();
    console.log('id', $(event.currentTarget).attr('data-id'))
    if (Session.get('currentUser')) {
    	Session.set('processTicket', 1)
    	Router.go('/schedule/' + $(event.currentTarget).attr('data-id'));
    } else {
    	alert('Please login first.');
    }
  }

});