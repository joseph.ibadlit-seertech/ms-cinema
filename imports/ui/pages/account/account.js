import './account.html'


Template.account.onCreated(function () {
	console.log('onCreated!!!')
	if (!Session.get('currentUser')) {
		Router.go('/home')
	}
})

Template.account.helpers({
	'user': function(){
		console.log('user')
	    return Users.findOne( { '_id':Session.get('currentUser')._id } )
	},
	'reservations': function() {
		return Reservations.find({ 'userId':Session.get('currentUser')._id })
	}
});


Template.account.events({

  // handle the form submission
  'submit form': function(event) {

    // stop the form from submitting
    event.preventDefault();
    console.log('event',event)
    console.log('target updateUser',event.target.updateUser)
    console.log('target updatePassword',event.target.updatePassword)
    console.log('Session', Session.get('currentUser'))
	
	var _id = Session.get('currentUser')._id;
	var query = { '_id':_id };
    if (event.target.updateUser) {
    	var firstName = event.target.firstName.value;
    	var updateDict = { $set: { 'firstName':firstName, 'lastName':event.target.lastName.value}}
    	console.log('updateDict', updateDict)
    	Users.update(query, updateDict)
    	Session.set('currentUser', { '_id':_id, 'firstName':firstName })
    	alert('Update successfully.')
    } else if (event.target.updatePassword) {
    	var currentPassword = Users.findOne({ '_id':_id }).password;
    	console.log('currentPassword', currentPassword)
    	if (event.target.currentPassword.value != currentPassword) {
	    	alert("Password did not match. Please try again.")
	    } else {
	    	Users.update(query, { $set: { 'password':event.target.newPassword.value } })
	    	event.target.currentPassword.value = '';
	    	event.target.newPassword.value = '';
	    	alert('Update successfully.')
	    }
    }
  }

});