import './schedule.html';
import './schedule.css';

Template.schedule.helpers({
    movie: function () {
        return Movies.findOne({'_id':Session.get('movieId')});
    }
});

Template.schedule.events({
  'change #branch-list': function(event) {
    // prevent the default behavior
    event.preventDefault();
    $('#schedule-list').removeAttr('disabled').empty();
  	$('#ticketDetails').addClass('hidden');
    var val = $(event.target).val();
    var movie = Template.schedule.__helpers.get('movie').call();

    var options = $("#schedule-list");
    var cinemas = movie.availability[val].cinemas;
    for (var x=0; x<cinemas.length; x++) {
      var schedule = cinemas[x].dateTime;
      options.append($("<option value='' disabled='' selected='' style='display:none;'/>").val('').text('Select Schedule'));
      for (i=0; i<schedule.length; i++) {
        options.append($("<option />").val(i).text(schedule[i].date + ' - ' + schedule[i].time + ' - ' + cinemas[x].name ));
      }
    }
  },
  'click #btnSelectTickets': function(event) {
  	event.preventDefault();
    console.log('event',event);

    var selectedSchedule = $('#schedule-list option:selected').text().split(' - ');
    var seatQty = $('#cboSeatsQty option:selected').text();

    var branch = $('#branch-list option:selected').text();
    var cinemas = Branches.findOne({'branch':branch}).cinemas;
    var seats;

    console.log('selectedSchedule[2]',(selectedSchedule[2]).toLowerCase())
    for (var i=0; i<cinemas.length; i++) {
      console.log('cinemas[i].name).toLowerCase()',(cinemas[i].name).toLowerCase())
      if ((cinemas[i].name).toLowerCase() == (selectedSchedule[2]).toLowerCase()) {
        seats = cinemas[i].seats;
        break;
      }
    }

    var reservations = Reservations.find({
      'branch':branch,
      'screening':$('#schedule-list option:selected').text()
    }).fetch();

    console.log('reservations',reservations);
    var seatsOccupied = reservations.reduce(function(sum, item) {
      console.log('sum item',sum, item);
      return sum + parseInt(item.qty);
    }, 0);

    var seatsLeft = seats - seatsOccupied;
    if ((seatsOccupied + parseInt(seatQty)) >= seats) {
      alert('There is only ' + (seats - seatsOccupied) + ' seats left. Please select another schedule.')
      return;
    }

    console.log('seatsOccupied',seatsOccupied)

    $('#seatsLeft').text(' (' + seatsLeft + ' seats left)');

  	$('#lblScreeningDate').text(selectedSchedule[0]);
  	$('#lblStartingTime').text(selectedSchedule[1]);

  	$('#lblSeatQtyTop').text(seatQty);
  	$('#lblSeatQtyBtm').text(seatQty);
  	$('#lblConvQty').text(seatQty);

  	var totalConv = parseInt(seatQty) * 20
  	$('#lblTotalConv').text('₱' + totalConv + '.00')
  	$('#ticketDetails').removeClass('hidden');

  	var totalTicketPrice = parseInt($('#lblPrice').text()) * parseInt(seatQty)
  	$('#lblTotalTicketPrice').text('₱' + totalTicketPrice + '.00');

  	$('#lblGrandTotal').text('₱' + (totalConv + totalTicketPrice) + '.00')
  },
  'click #btnPaymentGateway': function(event) {
    event.preventDefault();
  	if ($('input[name=optPayment]:checked').val() == 1) {
  		alert('Credit Card / Banchet payment option is under maintenance. Please choose other payment option.')
  	} else if ($('input[name=msterms]').is(":checked") == false) {
  		alert('If you wish to continue kindly check the MS Cinema Terms and Condtions.')
  	} else {
      var result = Reservations.insert({
        'userId':Session.get('currentUser')._id,
        'dateReserve':new Date().toString().split(' GMT')[0],
        'movieId': $('#btnSelectTickets').data("id"),
        'movie':$('#lblMovieName').text(),
        'branch':$('#branch-list option:selected').text(),
        'screening':$('#schedule-list option:selected').text(),
        'qty':parseInt($('#cboSeatsQty').val()),
        'status':'Pending'
      });

      if (result) {
        Router.go('/account');
        alert('Your movie ticket reservation was a success!');
      } else {
        alert('Something went wrong, please try again.')
      }
  	}
  }

});