import { Meteor } from 'meteor/meteor';
import { Router } from 'meteor/iron:router';

/*
 *  Utility layouts and navigation bars
 */

import '../../ui/pages/landing-page/landing-page.js';
import '../../ui/pages/home/home.js';
import '../../ui/pages/schedule/schedule.js';
import '../../ui/pages/account/account.js';
import '../../ui/pages/movie-list/movie-list.js';

// Router.configure({
//   layoutTemplate: 'navigation',
//   loadingTemplate: 'loading',
//   notFoundTemplate: 'pageNotFound'
// });

// Router.route('/', function () {
//   this.render('Home');
// });

Router.route( '/', {
  name            : 'landingPage',
  layoutTemplate  : 'landingPage',
	onBeforeAction  : function() {
    this.next();
  }
});

Router.route( '/schedule/:_id', {
  name            : 'schedule',
  layoutTemplate  : 'schedule',
  onBeforeAction  : function() {
	console.log('params',this.params)
	Session.set('movieId', this.params._id)
    this.next();
  }
});

Router.route( '/home', {
  name            : 'home',
  layoutTemplate  : 'home',
	onBeforeAction  : function() {
    this.next();
  }
});

Router.route( '/account', {
  name            : 'account',
  layoutTemplate  : 'account',
	onBeforeAction  : function() {
    this.next();
  }
});

Router.route( '/movie-list', {
  name            : 'movieList',
  layoutTemplate  : 'movieList',
  onBeforeAction  : function() {
    this.next();
  }
});
